.\" otpCalc is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; see the file COPYING.  If not, write to
.\" the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
.\"
.TH otpcalc 1 "2021-02-21" "otpcalc @VERSION@"
.SH "NAME"
otpcalc \- An OTP and S/Key calculator for X
.SH "SYNOPSIS"
.B otpcalc
.SH "DESCRIPTION"
\fBotpCalc\fP generates one time passwords for responding to S/Key (RFC1760) and OTP (RFC2289) challenges. It supports MD4, MD5, RIPEMD-160 and SHA1 message digests.
.LP
\fBotpCalc\fP determines which message digest to use based on the prefix to
the Challenge input:
.IP
.TS
allbox tab(#);
cb l
cb l
cb l
cb l
cb l.
s/key#MD5
otp-md4#MD4
otp-md5#MD5
otp-rmd160#RIPEMD-160
otp-sha1#SHA1
.TE
.LP
.LP
In the absence of a prefix, the default hash, specified in the Settings menu,
is used.
.SH "SEE ALSO"
.BR keyinit (1),
.BR donkey (1)
.SH "CONFORMING TO"
RFC 2289, RFC 1740
.SH "AUTHOR"
Anthony D. Urso <anthonyu@killa.net>.
.SH "COPYRIGHT"
Copyright \(co 2001 Anthony D. Urso.
.br
Copyright 2005\(en2021 Gentoo Authors
.br
This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
