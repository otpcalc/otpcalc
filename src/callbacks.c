/*
 * otpCalc - A one time password calculator.
 *
 * Copyright (C) 2001 Anthony D. Urso
 * Copyright 2006-2021 Gentoo Authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

#include "crypto.h"
#include "macros.h"
#include "utility.h"
#include "config.h"

void about(GtkWidget *widget, GtkWindow *parent)
{
	const gchar *authors[] = {
		"Anthony D. Urso",
		"Ulrich Müller <ulm@gentoo.org>",
		"Mart Raudsepp <leio@gentoo.org>",
		NULL
	};

	gtk_show_about_dialog(parent,
			      "version", VERSION,
			      "authors", authors,
			      "copyright", "Copyright (c) 2001, 2004 Anthony D. Urso\n"
					   "Copyright 2006-2021 Gentoo Authors",
			      "license-type", GTK_LICENSE_GPL_2_0,
			      "logo-icon-name", NULL,
			      "website", "https://gitlab.com/otpcalc/otpcalc",
			      "website_label", "Homepage",
			      "comments", "Original homepage:\n"
					  "http://www.killa.net/infosec/otpCalc/",
			      NULL);
}


void click(GtkWidget *unused, GtkWidget *button)
{

	gtk_button_clicked(GTK_BUTTON(button));

}

gboolean netwarn(void)
{
	int response;
	gchar *d;
	GtkWidget *di_netwarn, *la_netwarn;

	if (((d = getenv("DISPLAY")) == NULL) || g_str_has_prefix(d, ":"))
		return FALSE;

	di_netwarn = gtk_dialog_new_with_buttons("Warning!",
						 NULL,
						 0,
						 "_Yes", GTK_RESPONSE_YES,
						 "_No", GTK_RESPONSE_NO,
						 NULL);
	gtk_window_set_role(GTK_WINDOW(di_netwarn), "Warn");
	gtk_window_set_position(GTK_WINDOW(di_netwarn), GTK_WIN_POS_CENTER);

	la_netwarn = gtk_label_new(
		"It is unsafe to use otpCalc over an unencrypted X"
		" connection.\n\nProceed anyway?"
	);
	g_object_set(la_netwarn, "margin", 10, NULL);
	gtk_label_set_line_wrap(GTK_LABEL(la_netwarn), TRUE);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(
					GTK_DIALOG(di_netwarn))),
			  la_netwarn);

	gtk_dialog_set_default_response(GTK_DIALOG(di_netwarn),
					GTK_RESPONSE_NO);

	gtk_widget_show_all(di_netwarn);

	response = gtk_dialog_run(GTK_DIALOG(di_netwarn));

	gtk_widget_destroy(di_netwarn);

	return response != GTK_RESPONSE_YES;
}


void sethash(unsigned short new)
{

	extern unsigned short hash;

	hash = new;

}
