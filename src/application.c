/*
 * otpCalc - A one time password calculator.
 *
 * Copyright (C) 2001 Anthony D. Urso
 * Copyright 2007-2021 Gentoo Authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <gtk/gtk.h>

#include "application.h"
#include "callbacks.h"
#include "window.h"

struct _OtpcalcApplication
{
	GtkApplication parent_instance;
};

G_DEFINE_TYPE (OtpcalcApplication, otpcalc_application, GTK_TYPE_APPLICATION)

OtpcalcApplication *
otpcalc_application_new (void)
{
	return g_object_new (OTPCALC_TYPE_APPLICATION,
			     "application-id", "io.gitlab.otpcalc",
			     NULL);
}

static void
otpcalc_application_startup (GApplication *app)
{
	G_APPLICATION_CLASS (otpcalc_application_parent_class)->startup (app);

	if (netwarn()) {
		g_application_quit(G_APPLICATION(app));
		return;
	}

	otpcalc_window_new ();
}

static void
otpcalc_application_activate (GApplication *app)
{
	GtkWindow *window;

	if ((window = gtk_application_get_active_window (GTK_APPLICATION (app))))
		gtk_window_present_with_time(window,
					     g_get_monotonic_time() / 1000L);
}

static void
otpcalc_application_class_init (OtpcalcApplicationClass *klass)
{
	GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

	app_class->startup = otpcalc_application_startup;
	app_class->activate = otpcalc_application_activate;
}

static void
otpcalc_application_init (OtpcalcApplication *self)
{
}
