struct tokens {
	unsigned short alg;
	unsigned short seq;
	unsigned short sns;
	char *seed;
};

unsigned short parse(const char *, struct tokens *);
unsigned char parity(unsigned char *);
void sixwords(unsigned char *, char *);
