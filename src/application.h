/*
 * otpCalc - A one time password calculator.
 *
 * Copyright 2021 Gentoo Authors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define OTPCALC_TYPE_APPLICATION (otpcalc_application_get_type())

G_DECLARE_FINAL_TYPE (OtpcalcApplication, otpcalc_application, OTPCALC, APPLICATION, GtkApplication)

OtpcalcApplication *otpcalc_application_new (void);

G_END_DECLS
